## Репозиторий для рабочих файлов по байроновскому датасету

Ссылка на сведенную таблицу для совместной работы [в гугл-таблице](https://docs.google.com/spreadsheets/d/1rDlU948LFXbNR7WtXKypAsiN__oJPp0X94tM4tcDCm8/edit?usp=sharing)

Текущие задачи:
- Объединение таблиц по периодике и отд. изданиям (?)
- Соотнесение файлов пдф со строками метаданных, расстановка id в таблице метаданных
- Тег "распознано" для файлов с текстами / распознанными пдф
