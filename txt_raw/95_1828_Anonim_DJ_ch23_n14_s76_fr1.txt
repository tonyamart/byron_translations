В АЛЬБОМ.
(Подражание Байрону.)

Как хладный камень гробовой 
Вниманье наше пробуждает ,
Так пусть в стране тебе родной 
Мой стих о мне напоминает.

И пусть чрез много, много лет 
Души твоей хоть раз коснется;
Как первой радости привет,
На сердце сладко отзовется.

* * *

Русская песня.

Давно, красавица, стемнело,
А ты еще сидишь!
Уже в лампаде догорело,
Но ты не поглядишь!

*

Заря с востока занялася,
Румянит неба свод;
И мгла ночная разошлася;
Вмиг солнышко взойдет!
