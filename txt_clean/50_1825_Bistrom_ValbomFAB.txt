Как имя на доске холодной, гробовой,
Вниманье путника невольно привлекает -
Так остановится задумчивый взор твой,
Коль на пустынной сей странице повстречает -
Мое - забытое тобой!

Прочтешь и мысленно меня вспомянешь ты,
Как вспоминают тех, которых уж не стало.
Так! Здесь я схоронил заветные мечты
И сердце все мое: оно давно увяло
Среди надежд тщеты!
