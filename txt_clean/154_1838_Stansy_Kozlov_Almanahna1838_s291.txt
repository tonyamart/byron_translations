Глубоко в тишине, предав на век безмолвью,
Я тайну нежную храню в груди моей;
И сердце томное, к тебе дрожа любовью,
Вверяет лишь её одной любви твоей;

Под сводом тихая лампада гробовая
Бросает вечный свой никем незримый свет
Не тмит ее тоска, во мраке унывая,
Хотя напрасен блеск  как-будто вовсе нет

О, не забудь меня и близ моей могилы!
Увы, когда пройдешь, то вспомни милый прах
Один удар убьёт мои душевны силы,
Забвенья твоего ужасен сердцу страх

Будь тронут пламенной, нежнейшею мольбою
О тех, кого уж нет: печаль есть долг святой;
Обрадуй тень мою сердечною слезою,
Наградой за любовь, последнею — одной.
