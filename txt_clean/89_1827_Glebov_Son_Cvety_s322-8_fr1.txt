Друзья, внимайте: чудный сон! Готовьте долгое терпенье!
Зарею вспыхнул небосклон;
Проснулось к радости творенье;
От ветерка струится злак;
Цветы увлажены росою;
Свиваясь дымной пеленою,
Редеет на полянах мрак,
И восходящее светило
Все пышным блеском озарило.
Ручья по тучным берегам
Шел юноша, веселья полный;
Прозрачныя в потоке волны
Подобились его мечтам.
Ничто души не возмущало
Еще не знающей страстей:
Он другом был природе всей;
Но сердце пылко искало
Какой-то пищи для себя;
Оно любило - не любя,
И в наслаждении — желало!
На сопротивных берегах
Я видел юную девицу;
Она — приветствовать денницу
Шла также в радостных мечтах;
Венок из свежих роз свивала,
И улыбалась — и вздыхала!
Ея невинный, светлый взор
Каким-то полон был желаньем,
И тайный с сердцем разговор
Польстил каким-то ожиданьем.
Беспечный юноша поет
Свободы песню золотую,
Но видит прелесть молодую,
И вмиг цевница издает
Отзывы про любовь святую.
Вздыхают оба — чувства их
Слились в одно, казалось, чувство.
Притворства чуждо им искуство;
Как утра луч, их пламень тих;
Надежда, как струи зерцало;
Ни страх, ни подозрений жало
Любви не отравляют их. ...
Один поет — другая внемлет.
Певца невинность наградит:
Венок из рук ея летит,
Но лоно вод его приемлет. ...

Переменился чудный сон.

Средь мшистых скал, лесистый склон
Очам представил замок древний:
Лучь умирающий, вечерний
На шлицах башен угасал
И вратарь стражу окликал.
В сем замке сумрачном являлся
Унынья грознаго престол;
И сонный лес, и дикий дол
Лишь криком вранов оглашался,
И замка свод им отвечал.
Мне тот же юноша предстал;
Но злополучия след бурный
Страдальца исказили вид;
И омрачился взор лазурный,
И розы стерися с ланит,
И слезы льются сожаленья:
Он встречу с милой вспоминал.
Промчались годы — он увял!
И обманули наслажденья!
Но пробудился сердца глас;
Поет он песнь воспоминанью,
И вдохновение под час
Велиш затихнуть в нем страданью.
Плывешь из облака луна;
И замка смотрит из окна
Таж юная его подруга,
Но не его опа супруга!
Корыстолюбьем вручена
За золото любви притворной:
И день и ночь осуждена
Оплакивать свой плен позорной.
Она внимает глас певца;
Но строгий долг ей воспрещает
Уже невинный дар венца;
Певец, как призрак, убегает...
Переменялся чудный сон.
Опять является мпе он:
Не юношей, но возмужалым;
Издавна странником усталым
Скитается в стране чужой
Утрат с жестокою мечтой.
Во всех семействах гость минутный,
Задумчивый средь них пришлец,
Зрит сострадание сердец,
Но не покров от бед приютный;
И бродит из страны в страну,
Или вверяется пучинам;
Несется влажных гор к вершинам,
Иль бездны алчной в глубину.
Чуть теплится в нем пламень страсти,
Он к нежным чувствам охладел;
Внимали все, как прежде пел,
Никто не внемлет песнь напасти.
Сражает слух нестройный звук,
Отзыв души осиротелой,
И повестью сердечных мук
Скучает света круг веселой. ...

Переменился чудный сон.

В чертог роскошный пронесен,
Я зрел слиянье вкуса с златом;
И вновь явилась мне она,
Малютками окружена!
Рассыпясь на ковре богатом
Шумящим роем перед ней,
Они с беспечностью играли;
Но тусклый взор ея очей
Своей игрой не оживляли.
Вотще супруг, холодный к ней,
Стал попечительней, нежней,
В нем будто тень любви родилась:
Она с ним сердцем не делилась.
Вотще спешит веселья в храм;
Печаль по всем ея чертам
Запечатлела след глубокий,
И часто, с поприща утех,
Где царствовал нескромный смех,
Укроясь, слез лила потоки!
О чем? не ведала; но сны
Протекшаго являлись смутно:
Как средь коварной глубины
Исчез венок, польстив минутно,
Тому, кто был впервые мил,
И вот их жребий разлучил!
Венка свершилось прорицанье,
Красавицы удел — страданье!

Переменился чудный сон.

Явилась дебр — со всех сторон
Пустыни знойная равнина.
Страдалец тот же мне предстал,
И та же на челе кручина:
Но он спокойней сердцем стал.
Не жжет его ни солнца пламень,
He устрашает хлад ночной;
Безчувствен, как гранитный камень,
Среди окрестности немой.
Он весь в желаньях истощился,
Обнявшись с призраком любви;
И хлад убийственный струился
В медлительной его крови;
И сердце пламенем напрасным
Изпепелило жизнь свою,
С улыбкою и взором ясным
Стоял он бездны на краю. ...
В воображеньи одичалом.
Повсюду видел он хаос,
Но в сердце, к радостям увялом,
Стихийных на страшился гроз.
Узрев мир новый, чрезвычайный,
Незримых жителей небес
Он умолял потоком слез,
Чтоб бед источник обычайный
Навеки стер любви закон,
И ждал с благоговеньем он
Судеб непостижимых тайны...

Здесь кончился мой чудный сон.
