         Стансы.
      (Из Байрона.)
0, если бы вместо всех молний очей
 Дала бы ты нежности волю, 
Быть-можетъ они не будили-б страстей, 
 Но рай бы им выпал на долю.

Ты блещешь такою небесной красой, —
 Хотя и горят твои очи,—
Что мы повергаемся в прах пред тобой, 
 Но выдержать взгляда нет мочи.

Когда ты природой была создана,
 В тебе совершентсво блистало —
И долго, благая, боялась она, 
 Чтоб небо тебя не призвало.

Чтоб только избавить тебя—отвратить 
 От глаз твоих сонмы святые, 
Природа должна была пламя вложить
 В глаза до того неземные,

С тех пор загорелся твой взгляд роковой— 
 И смертного жжет и тревожит:
Он падает ниц пред твоей красотой,
 Но выдержать взгляда ее может.

В звезду превращенные светлой звездой,
 Горят волоса Береники;
Но нет тебе места меж ними: собой
 Затмила-б ты звездные лики.

Когда-б твои очи заискрились там,
 Созвездия меркнуть бы стали,
И самыя солнцы, подобно звездам,
 Едва бы на небе мерцали.

Н. Гербель.
2 ноября 1863.