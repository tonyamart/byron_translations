Она идет, сияя красотою,
Как звездна ночь в безоблачных краях;
Чем свет и тень пленяют нас собою,
Слилося то в лице ея, в очах —
В тот тихий свет, с той томностью живою,
Которых нет в блестящих дня лучах.
Придайте луч, одну лишь тень сильнее —
Волшебная затмится красота
И в локонах крыл ворона чернее,
И на лице, где каждая черта,
Весь блеск души передает живее,
Где помыслов яснеет чистота.
Величествен, спокоен лик прелестной,
Живым огнем пылает цвет ланит,
Улыбка уст,— след радости небесной, —
О бывших днях блаженства говорит.
Ея душе боренье неизвестно,
Чистейший огнь в груди ея горит.
