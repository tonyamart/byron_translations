                           СТАНСЫ.
                        (Из Байрона.)


Ни одна не станет в споре
     Красота с тобой,
И как музыка на море
     Сладок голос твой. 
Мope бурное смирилось,
Будто звукам покорилось,
Тихо лоно вод блестит,
Убаюкан ветер спит.

На морском дрожит просторе
     Луч луны блестя,
Тихо грудь вздымает море, 
     Как во сне дитя.
Так душа, полна вниманья, 
Пред тобой в очарованьи: 
Тихо все, но полно в ней, 
Будто летом, зыбь морей.
                                Н. ОГАРЕВ.