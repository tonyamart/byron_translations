ПОДРАЖАНИЕ ПОРТУГАЛЬСКОМУ.
(Из Байрона).

В час упоительный блаженства и любви, 
Когда любуюсь я тобою, друг прекрасный, 
«О жизнь моя!» уста прелестныя твои 
Мне шепчут сладостно с улыбкой неги страстной.

Хоть милыя слова гармонией живой, 
Звуча, в моей душе отрадно отдаются... 
Однако вспомни, друг, что жизнь — как сон пустой, 
И в жизни дни как сон мгновенно пронесутся... 
Что смерть, не пощадя прекрасных, юных лет 
Неумолимо все сразит своей рукою — 
И в мире человек исчезнет — будто след 
Забытый на песке и вымытый волною...

И лишь душа одна за гробом не умрет... 
Моя-ж любовь к тебе срослась с душой моею, — 
Она, как и душа, века переживет — 
Так лучше называй меня душой своею...