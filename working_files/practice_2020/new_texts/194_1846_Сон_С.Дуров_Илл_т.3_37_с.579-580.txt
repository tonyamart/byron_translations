СОН.
(ИЗ БАЙРОНА.)

А dream which is not ale a dream. Darkness.

1.
Жизнь двойственна: Наш сон, как жизнь, имеет 
Свой дивный мир. Его напрасно люди 
Зовут чертой граничной бытия 
С небытием. Сон—то же жизнь. Во сне мы 
Как на яву окружены мечтами, 
Исполненными жизни; мы горюем; 
Мы слезы льем и радуемся часто. 
Сон, иногда, пригрезившийся нам 
Волнует нас и после пробужденья, 
А иногда, он услаждает горе, 
Которое нас грызло на яву. 
Летучия и явственныя грезы, 
Как призраки прошедшаго, летят, 
Пророчествуя часто, как Сивиллы. 
Сны чудную имеют силу... В нас 
Они, то грусть, то радость пробуждают, 
Они творят из нас других людей 
По прихоти. Они пугают нас 
Минутными виденьями и тенью, 
Встающею и гибнущею тот час... 
Но боже мой! прошедшее—не сон ли? 
А что же сон такое? Сила духа, 
Способная воссоздавать.— Душа 
Творит миры несущие, и может 
Их населить такими существами, 
Которыя стократно выше нас — 
По времени и красоте и целям.

II.
Я видел два прелестных существа 
Во цвете лет. Они гуляли вместе 
По зелени пригорка : этот холм 
Казалось был кольцом последним—цепи 
Других холмов и выдавался мысом. 
Вокруг него не растилалось море, 
Но вместо волн смеялись луг и пашни;
А дальше лес, а по опушке леса 
Крестьянския избушки, из которых 
Клубами дым взвивался... 
Здесь-то были 
И юноша и девушка: она 
С любовию глядела на картины, 
Кругом ея раскинутыя... он же 
Ее одну, казалось, только видел. 
Они цвели красой и жизнью оба, 
Но разница заметная в летах 
Была видна меж ними. Как луна, 
Прорезавшись на крае небосклона, 
Она была к развитию близка 
И полному цветенью жизни. Он же 
Хотя отстал годами, но за то 
Опередил ее развитьем сердца... 
Все чудеса и прелести вселенной 
Слилися в ней для юноши, и он 
Вгляделся так в черты лица, что память 
Ее одну ему изображала... 
Он весь был в ней. Дышал ея дыханьем, 
Он видел все—ея глазами, думал — 
Ея умом... Отрывистое слово, 
Звук, брошенный на ветер ею—часто 
Рождали в нем какой-то знойный трепет... 
Он не жил сам: в ея существованьи 
Все бытие его сливалось. Мысли, 
Как бы ключи, стекались в океан 
Ея души. Пожатие руки, 
Звук голоса в нем разливали тот час 
Озноб и жар: то он бледнел, то яркий 
Румянец жег лице его. Едва-ли 
Он понимал, что было с ним...

Меж тем, 
Она была чужда его волненьям, 
И вздох ея летел к другому... Братом 
Она могла назвать его—и только... 
Но юноше и этаго довольно 
От существа, которое от сердца 
Звала его сим именем. 
Она была единственною ветьвью 
Угаснувшей и доблестной семьи.

Быть братом той, которую любил он — 
Он и хотел и не хотел... Зачем же? 
Года ему, потом, глаза открыли 
И грустную повысказали повесть...

III. 
Мой сон теперь внезапно изменился...

Я видел дом старинный. У ворот 
Ржал конь уже оседланный. В обширной 
Готической молельне, с беспокойством 
Взад и вперед тот юноша ходил... 
Но вот он сел, схватил перо и что-то 
На лоскутке бумаги написал... 
Потом склонясь печально головою, 
Он оперся и судорожно вздрогнул; 
Там, снова он встал с места—и письмо 
Вдруг разорвал—но слез над ним не пролил... 
Минуты две прошло—и он утих; 
Какое-то спокойствие святое 
Во всех чертах его заметно стало. 
Вдруг настежь дверь—и в комнату вошла 
С улыбкою и кротостью во взгляде, 
Та девушка, с которой он гулял. 
Она была чиста как ангел Божий, 
Но видела любовь его, но знала, 
Что тень она кидает на него, 
Что он страдал... а как страдал? едва ли 
Она могла понять его печали... 
Он подошел: Взял руку у нея 
И дружески пожал ее: в сей миг 
В его чертах, движеньях, в каждом взгляде 
Роилась тьма каких-то дум: не скоро 
С его лица все сгладилося. Руку, 
Он выпустил из рук своих, и вышел 
Из комнаты. Грозящая разлука 
Казалось их нисколько не смутила: 
С улыбкою они расстались... Двери 
Старинныя, со скрыпом отворились... 
Он вышел вон, вскочил на скакуна, 
И был таков!.. Уж только после 
Он не входил в те двери—никогда..

IV. 
Из юноши он мужем стал... Отчизной, 
Он избрал край далекий и пустынный, 
Где солнце жгло окрестность: это солнце 
С его душой согласовалось. Платье 
И весь наряд его был как-то странен, 
И сам он был, не тем, чем был он прежде. 
Он жизнь свою обрек волненьям: море, 
Леса и степь ему отчизной стали... 
Тьма образов, видений и картин 
Мне виделось... и всюду был он. После 
Явился он, в полдневный зной, ища 
Прохладнаго покоя: меж колонн, 
Под тенью стен, кругом обросших мохом, 
Своих творцев однако переживших 
Он лег, заснул. Не в далеке, 
От спящаго, паслись верблюды... дальше 
У звонкаго потока ржали кони — 
И человек, одетый в плащ широкий 
На страже был... кругом его лежала 
Толпа иноплеменцев. Кров над ними 
Был свод небес — и этот свод небес 
Был так хорош, лазурен и прекрасен, 
Что только Бог на нем бы мог явиться...

V. 
Мой сон опять внезапно изменился...

А девушка, которую любил он 
Была уже другому отдана... 
Она жила в отечестве... далеко 
От странника, блуждавшаго в чужбине. 
Вокруг нея, резвясь, играли дети, 
Прелестныя собою, как она: 
Но на лице у ней виднелась горесть, 
Тень внутренней борьбы; а на ресницах 
Слезинки пробивались... Боже мой! 
О чем бы ей печалиться, казалось? 
Тот близок к ней, кого она любила, 
А тот, кто сам ее любил безумно
Был далеко... он не взволнует боле 
Ея мечты преступною надеждой, 
И горестью своею не навеет 
Ей на сердце тревожнаго волненья... 
О чем же бы, казалось, горевать? 
Она любви его не отвечала, 
Она надежд ему не подала... 
В ея тоске он призрак отдаленный, 
Едва-едва очам мелькнувший сон...

VI. 
Мой сон опять внезапно изменился...

Изгнанник вновь на родине... Мне снится — 
Что в храме он стоит у алтаря 
С невестою... Она собой прекрасна, 
Но все не та, звезда любви начальной!... 
И между тем, когда идет обряд — 
Его лицо, покрыто тою ж тенью, 
Как некогда в молельне... Тот же трепет 
Заметен был в груди его... как прежде 
В глазах его роилась бездна дум 
Загадочных... но вот он как-то тише, 
Спокойней стал — и произнес обет. 
Но мнится мне, что в клятве нет сознанья... 
Что все идет кругом в глазах его, 
Что он совсем почти не помнит где он, 
И близь кого... В уме его преходят 
Старинный дом... ряд комнат... утро... вечер... 
И те места, где был он с ней когда-то 
Прошедшее явилось, словно призрак, 
Текущий миг закрыло перед ним... 
Бог весть зачем пришли воспоминанья 
Незваныя... зачем они пришли!...

VII. 
Мой сон опять внезапно изменился...

Та женщина, которую любил он, 
Была не той, чем некогда. Болезни 
Душевныя убили душу. Разум 
Затерян был. Глаза померкли. Взоры, 
Оторвались от дольняго. Как Фея 
Она жила в воздушном царстве. Мысли 
Несродныя одна с другой теснились 
В ея уме. Невидимыя нами 
Видения летали вкруг нея... 
И назвал свет ее — безумной. Но безумье — 
Печальный дар! Оно способно видеть 
Незримое для мудрости. Оно 
Яснее нас читает книгу жизни, 
Сближая нас, как телескоп с природой. 
Убив мечты, безумье нас знакомит 
С той истиной, которую рассудок 
В цветистыя одежды облекает...

VIII.
Мой сон опять внезапно изменился. 
Скиталец мой опять один. Те люди, 
Которые теснились вкруг него, — 
Одни его оставили, другие 
К нему пришли с открытою враждой. 
Преследуем обидами и злобой  
Он жертвой стал несчастий. Тяжким горем 
Отравлено все было для него : 
Как Митридат он стал питаться ядом, 
И этот яд над ним утратив силу, 
Стал, наконец, его любимой пищей: 
Он жизнь нашел, где видят смерть другие... 
И сделались его друзьями — горы, 
И звезды с ним беседу повели, 
Незримые владыки мира, духи 
Ему в тиши рассказывали повесть 
О чудесах вселенной. Ночь ему 
Открыла грудь. Из бездн стремнин глубоких 
Ему шептал какой-то дивный голос 
О чудесах и таинствах подземных...

С. Дуров.


