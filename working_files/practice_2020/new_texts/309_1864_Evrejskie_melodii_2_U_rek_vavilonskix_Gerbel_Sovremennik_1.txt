II.
У рек вавилонских.

У рек вавилонских мы, сидя, с тоской, 
В слезах вспоминали тот день роковой, 
Как рать вавилонян, отмщеньем томима, 
Во прах сокрушила твердыни Солима, 
И девы Сиона, с слезами в очах, 
Себя увидали в далеких землях.

В то время, как — мыслей нерадостных полны — 
Мы с грустью следили бежавшия волны, 
Враги заставляли нас петь и играть: 
Напрасно! — им песен святых не слыхать! 
Пусть прежде отсохнет рука над струнами, 
Чем радостный звук извлечет пред врагами!

Сион, твои арфы висят на ветвях! 
Свободная песня в свободных струнах — 
Нам стала заветом годины кровавой, 
Видавшей конец твой, увенчанный славой... 
Нет, звуков твоих мы во век не сольём 
С безумною песней, сложенной врагом!