ѴШ.
Мне призрак явился.
(из Иова).

Мне призрак явился — и я без покрова 
Бессмертье увидел... На смертных пал сон, 
Лишь я отвратить от пришельца святова
Не мог своих глаз, хоть безплотен был он... 
И дрогнуло тело, и дыбом стал волос... 
И слуха коснулся божественный голос:

«Ужель человек справедливее Бога; 
«Когда серафимы — подножье Его? 
«Вы червь долговечный, вы прах от порога, — 
«А лучше ль, правдивее ль вы от того? 
«Создание дня, вы живете до ночи: 
«Пред мудрости светом слепотствуют очи...»

Н. Гербель
